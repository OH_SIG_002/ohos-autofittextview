/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { TypedValue } from './TypedValue'

@ComponentV2
struct AutofitTextView {
  @Param @Require model: AutofitTextView.Model;
  @Event $model: (val: number) => void = (val: number) => {};

  build() {
    Text(this.model.content)
      .fontSize(this.model.textSize)
      .minFontSize(this.model.minTextSize)
      .maxFontSize(this.model.maxTextSize)
      .maxLines(this.model.maxLines)
      .textAlign(TextAlign.Center)
      .backgroundColor(this.model.backgroundColor)
      .width(this.model.width)
      .textOverflow({ overflow: this.model.overFlow })
  }
}

namespace AutofitTextView {
  @ObservedV2
  export class Model {
    @Trace textSize: ESObject= 20
    @Trace minTextSize: ESObject= 10
    @Trace maxTextSize: ESObject= 40
    @Trace maxLines: number= 1
    @Trace content: string= ''
    @Trace width: string= '100%'
    @Trace isEnabled: boolean= true
    @Trace precision: number= 10
    @Trace mMaxTextSize: number= 40
    @Trace mMinTextSize: number= 20
    @Trace backgroundColor: number= 0XFFFFFF
    @Trace overFlow: TextOverflow = TextOverflow.Clip

    setTextSize(size: number, unit ?: number): Model {
      if (unit == undefined) {
        this.textSize = size
      } else if (unit == TypedValue.COMPLEX_UNIT_PX) {
        this.textSize = size + 'px'
      } else if (unit == TypedValue.COMPLEX_UNIT_FP) {
        this.textSize = size + 'fp'
      }
      return this
    }

    setMaxLines(lines: number): Model {
      this.maxLines = lines
      return this
    }

    setBackgroundColor(color: number): Model{
      this.backgroundColor = color
      return this
    }

    setTextOverflow(flow: number): Model{
      this.overFlow = flow
      return this
    }

    setText(text: string): Model {
      this.content = text
      return this
    }

    setWidth(width: string): Model{
      this.width = width
      return this
    }

    public isSizeToFit(): boolean {
      return this.isEnabled;
    }

    public setSizeToFit(sizeToFit : boolean): Model {
      this.isEnabled = sizeToFit;
      return this
    }

    public getMaxTextSize(): number {
      return this.mMaxTextSize
    }

    public setMaxTextSize(maxSize: number, unit ?: number): Model {
      this.mMaxTextSize = maxSize
      if (unit == undefined) {
        this.maxTextSize = maxSize
      } else if (unit == TypedValue.COMPLEX_UNIT_PX) {
        this.maxTextSize = maxSize + 'px'
      } else if (unit == TypedValue.COMPLEX_UNIT_FP) {
        this.maxTextSize = maxSize + 'fp'
      }
      return this
    }

    public getMinTextSize(): number{
      return this.mMinTextSize
    }

    public setMinTextSize(minSize: number, unit ?: number): Model {
      this.mMinTextSize = minSize
      if (unit == undefined) {
        this.minTextSize = minSize
      } else if (unit == TypedValue.COMPLEX_UNIT_PX) {
        this.minTextSize = minSize + 'px'
      } else if (unit == TypedValue.COMPLEX_UNIT_FP) {
        this.minTextSize = minSize + 'fp'
      }
      return this
    }

    public getPrecision(): number {
      return this.precision
    }

    public setPrecision(precision: number): Model {
      this.precision = precision
      return this
    }

    public onTextSizeChange(textSize: number, oldTextSize: number): void {
      // do nothing
    }
  }
}

export { AutofitTextView };
